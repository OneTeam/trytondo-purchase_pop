#The COPYRIGHT file at the top level of this repository contains the full
#copyright notices and license terms.
from trytond import backend
from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.tools.multivalue import migrate_property

__all__ = ['Configuration', 'ConfigurationSequence']

def default_func(field_name):
    @classmethod
    def default(cls, **pattern):
        return getattr(
            cls.multivalue_model(field_name),
            'default_%s' % field_name, lambda: None)()
    return default


class Configuration(metaclass=PoolMeta):
    __name__ = 'purchase.configuration'
    pop_sequence = fields.MultiValue(fields.Many2One(
        'ir.sequence.strict', "Purchase POP Sequence",
        domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('sequence_type.name', '!=', 'Purchase Pop'),
            ],
        depends=['company', 'sequence_type']))
    withholding_tax = fields.Many2One('account.tax', 'Withholding Tax',
                                      domain=[
                                          ('rate', '<', 0),
                                          ('group.kind', '=', 'purchase'),
                                      ],
                                      depends=['rate', 'group'])
    withholding_tax_amount = fields.Numeric('Withholding Tax Amount', digits=(16, 2))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'pop_sequence':
            return pool.get('purchase.configuration.sequence')
        return super(Configuration, cls).multivalue_model(field)

    default_pop_sequence = default_func('pop_sequence')


class ConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'purchase.configuration.sequence'
    pop_sequence = fields.Many2One(
        'ir.sequence.strict', "Purchase POP Sequence", required=True,
        domain=[
            ('company', 'in', [Eval('company', -1), None]),
            ('sequence_type.name', '!=', 'Purchase Pop'),
            ],
        depends=['company', 'sequence_type'])

    @classmethod
    def __register__(cls, module_name):
        exist = backend.TableHandler.table_exist(cls._table)

        super(ConfigurationSequence, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('pop_sequence')
        value_names.append('pop_sequence')
        fields.append('company')
        migrate_property(
            'purchase.configuration', field_names, cls, value_names,
            fields=fields)

    @classmethod
    def default_pop_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('purchase_pop', 'sequence_purchase_pop')
        except KeyError:
            return None
