# This file is part of purchase_pop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from decimal import Decimal
from datetime import datetime
from trytond.model import ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.pyson import Bool, Eval, Or
from trytond.wizard import (Wizard, StateView, StateReport, StateTransition,
    Button)
from trytond.modules.company import CompanyReport
from functools import reduce
from trytond.exceptions import UserError
from trytond.modules.product import price_digits

_ZERO = Decimal('0.00')

SAMPLE = [
    ('7000', '100g'),
    ('10500', '150g'),
    ('14000', '200g'),
    ('17500', '250g'),
    ('21000', '300g'),
]

_digits = (16, 2)

_STATES_REQUIRED = {
    'readonly': Bool(Eval('lines')),
    'required': Eval('sample') != 'None' and
    Bool(Eval('make_sample')),
    'invisible': Bool(~Eval('make_sample')),
}

_STATES_REQUIRED_ALL = {
    'readonly': Bool(Eval('lines')),
    'required': Eval('sample') != 'None',
}

_STATES_READONLY = {
    'readonly': Eval('sample') != 'None',
    'invisible': Bool(~Eval('make_sample')),
}

_STATES_READONLY_ALL = {
    'readonly': Eval('sample') != 'None',
}

_CERTIFICATE = [
    ('cafe_practices', 'C.A.F.E Practices'),
    ('stantdard', 'Estandar'),
    ('rainforest', 'Rainforest'),
    (None, ''),
]

_DEPENDS_RE = ['bonus_performance', 'price_certificate', 'base_price',
               'bonus_points', 'sample', 'impurities', 'bonus_total',
               'price_end', 'certificate', 'make_sample']

class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    ticket_number = fields.Char('Ticket Number', readonly=True, select=True)
    self_pick_up = fields.Boolean('Self Pick Up', states={
            'readonly': Eval('state') != 'draft',
            }, depends=['state'],
        help='The goods are picked up by the customer before the purchase, so no '
        'shipment is created.')
    pop_create_date = fields.DateTime('Create Date', readonly=True)
    allow_credit_device = fields.Function(fields.Boolean('Allow Credit'),
                                          'get_device_allow_credit')
    certificate = fields.Selection(_CERTIFICATE, 'Certificate')
    sample = fields.Selection(SAMPLE, 'Sample',
                                states=_STATES_REQUIRED,
                                depends=['lines'])
    decrease = fields.Numeric('Decrease', _digits,
                                states=_STATES_REQUIRED,
                                depends=['lines'])
    impurities = fields.Numeric('Impurities', _digits,
                                states=_STATES_REQUIRED,
                                depends=['lines'])
    performance_rate = fields.Numeric('Performance Rate', _digits,
                                      states=_STATES_READONLY)
    bonus_points = fields.Numeric('Bonus Points', _digits,
                                  states=_STATES_READONLY)
    price_certificate = fields.Numeric('Price Certificate', _digits,
                                states= {
                                    'readonly': (Bool(~Eval('certificate')) |
                                                 Bool(Eval('lines'))),
                                    'required': Bool(Eval('certificate')),},
                                depends=['lines', 'certificate'])
    price_certificate_charge = fields.Numeric('Price Certificate Charge', _digits,
                                              states=_STATES_READONLY_ALL)          
    bonus_total = fields.Numeric('Bonus Total', _digits,
                                  states=_STATES_READONLY)
    bonus_performance = fields.Numeric('Bonus Performance', _digits,
                                       states=_STATES_READONLY)
    price_kilo = fields.Numeric('Price Kilo', _digits,
                                states=_STATES_REQUIRED_ALL,
                                depends=['lines'])
    price_charge =  fields.Numeric('Price Charge', _digits,
                                   states=_STATES_READONLY_ALL)                                  
    price_end = fields.Numeric('Price End', _digits,
                                         states={
                                             'readonly': Eval('sample') != 'None',
                                             'required': Eval('sample') != 'None',
                                         })
    decrease_percentage = fields.Numeric('Percentage Decrease', _digits,
                                         states={
                                             'readonly': Eval('sample') != 'None',
                                             'required': Bool(Eval('make_sample')),
                                             'invisible': Bool(~Eval('make_sample')),
                                         })
    make_sample = fields.Boolean('Make Sample',
                                 states={
                                         'readonly': Bool(Eval('lines')),
                                 })
 

    @classmethod
    def __register__(cls, module_name):
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()

        super(Purchase, cls).__register__(module_name)
        cursor.execute(*sql_table.update(
                columns=[sql_table.pop_create_date],
                values=[sql_table.create_date],
                where=sql_table.pop_create_date == None))

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls.party.depends.extend(['shops'])
        cls.party.domain = [('shops.id', 'in', Eval('context', {}).get('shops', []))
                            ]
        cls.lines.states  = {'readonly': Bool(~Eval('price_end')),
                             }
        cls.lines.depends.extend(['price_end'])
        
        for fname in ('invoice_method', 'invoice_address'):
            fstates = getattr(cls, fname).states
            if fstates.get('readonly'):
                fstates['readonly'] = Or(fstates['readonly'],
                    Eval('self_pick_up', False))
            else:
                fstates['readonly'] = Eval('self_pick_up', False)
            getattr(cls, fname).depends.append('self_pick_up')
        if hasattr(cls, 'carrier'):
            if 'invisible' not in cls.carrier.states:
                cls.carrier.states['invisible'] = Bool(Eval('self_pick_up'))
            else:
                invisible = cls.carrier.states['invisible']
                cls.carrier.states['invisible'] = Or(invisible,
                    Bool(Eval('self_pick_up')))

        cls._buttons.update({
                'add_sum': {
                    'invisible': Eval('state') != 'draft'
                    },
                'wizard_add_product': {
                    'invisible': Eval('state') != 'draft'
                    },
                'print_ticket': {},
                'full_to_done': {
                    'invisible': ~Eval('state').in_(['draft']) or ~Eval('allow_credit_device', False),
                    'readonly': ~Eval('lines', []),
                    'depends': ['allow_credit_device', 'state'] 
                    },
                })
        for fname in ('self_pick_up', 'currency', 'party'):
            if fname not in cls.lines.on_change:
                cls.lines.on_change.add(fname)

    @staticmethod
    def default_party():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.party.id if user.shop and user.shop.party else None
    
    @staticmethod
    def default_allow_credit_device():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if user.purchase_device:
            return user.purchase_device.allow_credit

    @classmethod
    def default_sample(cls):
        return '21000'

    @classmethod
    def default_decrease(cls):
        return 0
    
    @classmethod
    def default_impurities(cls):
        return 0

    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate', 'sample',
                    'price_certificate_charge', 'decrease', 'decrease_percentage')
    def on_change_with_sample(self):
        self.get_performance_rate_recalculate(self)
        if self.sample == '7000':
            return '7000'
        elif self.sample == '10500':
            return '10500'
        
        elif self.sample == '14000':
            return '14000'

        elif self.sample == '17500':
            return '17500'
        else:
            return '21000'
                    
    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge', 'decrease', 'decrease_percentage')                
    def on_change_decrease(self):
        if self.sample == '21000':
            if self.decrease < 0 or self.decrease > 300:
                self.decrease = 0
            self.get_performance_rate_recalculate(self)
        elif self.sample == '17500':
            if self.decrease < 0 or self.decrease > 250:
                self.decrease = 0
            self.get_performance_rate_recalculate(self)
        elif self.sample == '14000':
            if self.decrease < 0 or self.decrease > 200:
                self.decrease = 0
            self.get_performance_rate_recalculate(self)
        elif self.sample == '10500':
            if self.decrease < 0 or self.decrease > 150:
                self.decrease = 0
            self.get_performance_rate_recalculate(self)
        else:
            if self.decrease < 0 or self.decrease > 100:
                self.decrease = 0
            self.get_performance_rate_recalculate(self)
    
    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge', 'decrease', 'decrease_percentage')
    def on_change_impurities(self):
        self.get_performance_rate_recalculate(self)


    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge', 'decrease', 'decrease_percentage')
    def on_change_sample(self):
        self.get_performance_rate_recalculate(self)

    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge', 'decrease', 'decrease_percentage')
    def on_change_price_kilo(self):
        self.get_performance_rate_recalculate(self)

    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge','decrease', 'decrease_percentage')
    def on_change_price_certificate(self):
        self.get_performance_rate_recalculate(self)

    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge','decrease', 'decrease_percentage')        
    def on_change_certificate(self):
        self.get_performance_rate_recalculate(self)

    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge','decrease', 'decrease_percentage')        
    def on_change_price_charge(self):
        self.get_performance_rate_recalculate(self)

    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge','decrease', 'decrease_percentage')        
    def on_change_price_certificate_charge(self):
        self.get_performance_rate_recalculate(self)

    @fields.depends('bonus_performance', 'price_kilo', 'certificate', 'sample',
                    'bonus_points', 'make_sample', 'impurities', 'bonus_total',
                    'price_end', 'price_charge', 'price_certificate',
                    'price_certificate_charge','decrease', 'decrease_percentage')        
    def on_change_make_sample(self):
        self.get_performance_rate_recalculate(self)
                
    @fields.depends(methods=['on_change_self_pick_up'])
    def on_change_shop(self):
        super(Purchase, self).on_change_shop()
        if self.shop:
            self.self_pick_up = self.shop.self_pick_up
            self.on_change_self_pick_up()

    @fields.depends(methods=['on_change_self_pick_up'])
    def on_change_party(self):
        super(Purchase, self).on_change_party()
        if hasattr(self, 'self_pick_up') and self.self_pick_up:
            self.on_change_self_pick_up()

    @fields.depends('self_pick_up', 'shop', 'party')
    def on_change_self_pick_up(self):
        if self.self_pick_up:
            self.invoice_method = 'order'
            self.shipment_method = 'order'
            if self.shop and self.shop.address:
                self.shipment_address = self.shop.address
            if hasattr(self, 'carrier'):
                self.carrier = None
        else:
            self.invoice_method = self.default_invoice_method()
            
    @fields.depends('purchase_device')
    def get_device_allow_credit(self, name):
        if self.purchase_device:
            return self.purchase_device.allow_credit

    def get_performance_rate_recalculate(self, name):
        if self.make_sample:
            if self.sample == '10500' and self.decrease > 150:
                self.decrease = 0
                self.impurities = 0
            if not self.certificate:
                self.price_certificate = _ZERO
            if self.price_kilo:
                self.price_charge = self.price_kilo * 125
            else:
                self.price_charge = None
            if self.price_certificate:
                self.price_certificate_charge = self.price_certificate * 125
            else:
                self.price_certificate_charge = None
            if self.impurities > 0 and self.impurities <= self.decrease:
                performance_rate = (Decimal(self.sample)/self.impurities)/100
                self.performance_rate = performance_rate
                self.bonus_points = Decimal(0.94) - performance_rate
                if self.sample == '21000' and self.decrease > 0:
                    self.decrease_percentage = Decimal(1)-(Decimal(self.decrease)/Decimal(300))
                elif self.decrease > 0:
                    self.decrease_percentage = Decimal(1)-(Decimal(self.decrease)/Decimal(150))
                else:
                    self.impurities = 0
                    self.decrease = 0
                    self.decrease_percentage = 0
            else:
                self.impurities = 0
                self.performance_rate = 0
                self.bonus_points = None
                self.decrease_percentage = None
                self.price_end = None
                self.bonus_performance = None
            if self.bonus_points != None and self.price_kilo != None:
                self.bonus_performance  = (self.price_kilo * self.bonus_points)
            if self.bonus_performance != None and self.price_certificate != None:
                self.bonus_total  = self.bonus_performance + self.price_certificate
                if self.price_kilo:
                    self.price_end = self.bonus_total + self.price_kilo
        else:
            if self.price_kilo:
                self.price_charge = self.price_kilo * 125
            else:
                self.price_charge = None                
            if self.price_certificate:
                self.price_certificate_charge = self.price_certificate * 125
            else:
                self.price_certificate_charge = None
            if self.price_kilo and self.price_certificate:
                self.price_end = self.price_kilo + self.price_certificate
            elif self.price_kilo:
                self.price_end = self.price_kilo
            self.impurities = 0
            self.performance_rate = 0
            self.bonus_points = None
            self.decrease_percentage = None
            self.bonus_performance = None

    @classmethod
    def view_attributes(cls):
        return super(Purchase, cls).view_attributes() + [
            ('//group[@id="full_workflow_buttons"]', 'states', {
                    'invisible': Eval('self_pick_up', False),
                    })]

    @classmethod
    def create(cls, vlist):
        now = datetime.now()
        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            vals['pop_create_date'] = now
        return super(Purchase, cls).create(vlist)

    @classmethod
    def copy(cls, purchases, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['ticket_number'] = None
        return super(Purchase, cls).copy(purchases, default=default)

    @classmethod
    @ModelView.button_action('purchase_pop.wizard_add_product')
    def wizard_add_product(cls, purchases):
        pass

    @classmethod
    @ModelView.button
    def add_sum(cls, purchases):
        Line = Pool().get('purchase.line')
        lines = []
        for purchase in purchases:
            line = Line(
                purchase=purchase.id,
                type='subtotal',
                description='Subtotal',
                sequence=10000,
                )
            lines.append(line)
        Line.save(lines)

    @classmethod
    @ModelView.button_action('purchase_pop.report_purchase_ticket')
    def print_ticket(cls, purchases):
        pool = Pool()
        Config = pool.get('purchase.configuration')

        config = Config(1)
        for purchase in purchases:
            if (not purchase.ticket_number and
                    purchase.residual_amount == Decimal('0.0')):
                purchase.ticket_number = config.pop_sequence.get()
                purchase.save()

    def create_shipment(self, shipment_type):
        if self.self_pick_up:
            return self.create_moves_without_shipment(shipment_type)
        return super(Purchase, self).create_shipment(shipment_type)

    def create_moves_without_shipment(self, shipment_type):
        Move = Pool().get('stock.move')

        if not self.self_pick_up:
            return

        assert self.shipment_method == 'order'

        moves = []
        for line in self.lines:
            move = line.get_move(shipment_type)
            if move:
                moves.append(move)
        if moves:
            moves = Move.create([m._save_values for m in moves])
            Move.do(moves)

        self.set_shipment_state()

    @fields.depends('lines', 'currency', 'party', 'self_pick_up')
    def on_change_lines(self):
        '''
        Overrides this method completely if the purchase is self pick up to improve
        performance: Computes untaxed, total and tax amounts from the already
        computed values in purchase lines.
        '''
        if not self.self_pick_up:
            super(Purchase, self).on_change_lines()

        self.untaxed_amount = Decimal('0.0')
        self.tax_amount = Decimal('0.0')
        self.total_amount = Decimal('0.0')

        if self.lines:
            self.untaxed_amount = reduce(lambda x, y: x + y,
                [(getattr(l, 'amount', None) or Decimal(0))
                    for l in self.lines if l.type == 'line'], Decimal(0)
                )
            self.total_amount = reduce(lambda x, y: x + y,
                [(getattr(l, 'amount_w_tax', None) or Decimal(0))
                    for l in self.lines if l.type == 'line'], Decimal(0)
                )
        if self.currency:
            self.untaxed_amount = self.currency.round(self.untaxed_amount)
            self.total_amount = self.currency.round(self.total_amount)
        self.tax_amount = self.total_amount - self.untaxed_amount
        if self.currency:
            self.tax_amount = self.currency.round(self.tax_amount)

    @classmethod
    @ModelView.button
    def full_to_done(cls,purchases):
        '''Lleva las ventas desde el estado borrador(draft) hasta el estado finalizado(done).
           En estre proceso crea la factura y los movimientos de stock de forma automática,
           además de aprobarlos y contabilizarlos.'''
        
        Invoice = Pool().get('account.invoice')
        Shipment = Pool().get('stock.shipment.out')
        ShipmentReturn = Pool().get('stock.shipment.out.return')
        Move = Pool().get('stock.move')
        Configuration = Pool().get('purchase.configuration')
        configuration = Configuration(1)
        
        if purchases:
            for purchase in purchases:
                if len(purchase.lines)<1:
                    cls.raise_user_error('La venta{s} no está en estado borrador o no contiene lineas'.foramt(s=purchase))
                    return
        else:
            cls.raise_user_error('Error en las venatas seleccionadas')
            return
        
        cls.quote(purchases)
        cls.confirm(purchases)
        cls.process(purchases)
        Transaction().commit()#se graba la informacion para poder acceder a las facturas y los shipments
        purchases = cls.browse([x.id for x in purchases])#se vuelve a leer para cargar informacion de la base de datos, verificar si es necesario.
        purchases = [x for x in purchases if (x.state == 'processing' and len(x.lines) >0 ) ]#solo las ventas en borrador
        if len(purchases) <= 0:
            cls.raise_user_error('Falló la finalización automática de la(s) venta(s)')
            return
        for purchase in purchases:
            invoices = purchase.invoices
            if invoices:
                for invoice in invoices:
                    if not invoice.invoice_date:
                        invoice.invoice_date = purchase.purchase_date
                        Invoice.save([invoice])
                Invoice.validate_invoice(invoices)
                Invoice.post(invoices)
            shipments = purchase.shipments
            if shipments:
                for shipment in shipments:
                    shipment.effective_date = purchase.purchase_date
                    Shipment.save([shipment])
                Shipment.assign_wizard(shipments)
                Shipment.pack(shipments)
                Shipment.done(shipments)
            shipment_returns = purchase.shipment_returns
            if shipment_returns:
                for shipment_return in shipment_returns:
                    shipment_return.effective_date = purchase.purchase_date
                    ShipmentReturn.save([shipment_return])
                ShipmentReturn.receive(shipment_returns)
            moves = purchase.moves
            if moves:
                for move in moves:
                    if move.shipment:
                        continue
                    if move.to_location != configuration.one_click_to_location:
                        move.to_location = configuration.one_click_to_location
                    move.effective_date = purchase.purchase_date
                    Move.save([move])
                    Move.do([move])
            cls.process([purchase])
                
class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    change_unit_price_device = fields.Function(
        fields.Boolean('Change Unit Price'),
        'get_device_change_unit_price')
    bonus_performance = fields.Numeric('Bonus Performance', _digits,
                                       states = {
                                           'readonly': ~Eval('change_unit_price_device', True),
                                       })
    price_kilo = fields.Numeric('Price Kilo', _digits,
                                states = {
                                    'readonly': ~Eval('change_unit_price_device', True),
                                })
    
    @classmethod
    def __setup__(cls):
        super(PurchaseLine, cls).__setup__()
        cls.unit_price.depends += ['change_unit_price_device']
        cls.unit_price.states.update({
            'readonly': ~Eval('change_unit_price_device', True),
            })

    @staticmethod
    def default_purchase():
        if Transaction().context.get('purchase'):
            return Transaction().context.get('purchase')
        return None

    @staticmethod
    def default_change_unit_price_device():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if user.purchase_device:
            return user.purchase_device.change_unit_price

    @fields.depends('purchase', '_parent_purchase.shop', 'analytic_accounts')        
    def on_change_product(self):
        Purchase = Pool().get('purchase.purchase')

        if not self.purchase:
            purchase_id = Transaction().context.get('purchase')
            if purchase_id:
                self.purchase = Purchase(purchase_id)
        super(PurchaseLine, self).on_change_product()
        self.analytic_accounts = ()
        try:
            self.analytic_accounts += ({'root': self.purchase.shop.analytic_root, 
                                        'account': self.purchase.shop.analytic_account},) 
        except:
            raise UserError(str("Debe seleccionar 'Raíz' y 'Cuenta'"))

    @fields.depends('purchase', 'unit_price', '_parent_purchase.price_end', 'quantity',
                    '_parent_purchase.price_kilo', '_parent_purchase.performance_rate',
                    '_parent_purchase.shop', 'taxes')
    def on_change_quantity(self):
        Purchase = Pool().get('purchase.purchase')
        Config = Pool().get('purchase.configuration')
        config = Config(1)
        if not self.purchase:
            purchase_id = Transaction().context.get('purchase')
            if purchase_id:
                self.purchase = Purchase(purchase_id)
        super(PurchaseLine, self).on_change_quantity()
        self.unit_price = self.purchase.price_end
        self.bonus_performance = self.purchase.performance_rate
        self.price_kilo = self.purchase.price_kilo
        if config.withholding_tax_amount <= (Decimal(self.quantity) * self.purchase.price_end):
            if self.purchase.shop.withholding_tax:
                self.taxes = [self.purchase.shop.withholding_tax]
            elif config.withholding_tax:
                self.taxes = [config.withholding_tax]
            else:
                raise UserError(str('Debe tener una cuenta de impuestos'))
        
    @fields.depends('purchase')
    def on_change_with_amount(self):
        if not self.purchase:
            self.purchase = Transaction().context.get('purchase')
        return super(PurchaseLine, self).on_change_with_amount()

    @fields.depends('purchase')
    def get_device_change_unit_price(self, name):
        return self.purchase.purchase_device.change_unit_price

    def get_from_location(self, name):
        res = super(PurchaseLine, self).get_from_location(name)
        if self.purchase.self_pick_up:
            if self.warehouse and self.quantity >= 0:
                return self.warehouse.storage_location.id
        return res

    def get_to_location(self, name):
        res = super(PurchaseLine, self).get_to_location(name)
        if self.purchase.self_pick_up:
            if self.warehouse and self.quantity < 0:
                return self.warehouse.storage_location.id
        return res


class StatementLine(metaclass=PoolMeta):
    __name__ = 'account.statement.line'
    purchase = fields.Many2One('purchase.purchase', 'Purchase', ondelete='RESTRICT')


class PurchaseTicketReport(CompanyReport, metaclass=PoolMeta):
    __name__ = 'purchase_pop.purchase_ticket'


class PurchaseReportSummary(CompanyReport, metaclass=PoolMeta):
    __name__ = 'purchase_pop.purchases_summary'

    @classmethod
    def get_context(cls, records, data):
        report_context = super(PurchaseReportSummary, cls).get_context(records, data)

        sum_untaxed_amount = Decimal(0)
        sum_tax_amount = Decimal(0)
        sum_total_amount = Decimal(0)
        for purchase in records:
            sum_untaxed_amount += purchase.untaxed_amount
            sum_tax_amount += purchase.tax_amount
            sum_total_amount += purchase.total_amount

        report_context['sum_untaxed_amount'] = sum_untaxed_amount
        report_context['sum_tax_amount'] = sum_tax_amount
        report_context['sum_total_amount'] = sum_total_amount
        report_context['company'] = report_context['user'].company
        return report_context


class AddProductForm(ModelView):
    'Add Product Form'
    __name__ = 'purchase_pop.add_product_form'
    purchase = fields.Many2One('purchase.purchase', 'Purchase')
    lines = fields.One2Many('purchase.line', None, 'Lines',
        context={
            'purchase': Eval('purchase'),
            },
        depends=['purchase'],)


class WizardAddProduct(Wizard):
    'Wizard Add Product'
    __name__ = 'purchase_pop.add_product'
    start = StateView('purchase_pop.add_product_form',
        'purchase_pop.add_product_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Add and New', 'add_new_', 'tryton-go-jump', default=True),
            Button('Add', 'add_', 'tryton-ok'),
        ])
    add_new_ = StateTransition()
    add_ = StateTransition()

    def default_start(self, fields):
        return {
            'purchase': Transaction().context.get('active_id'),
            }

    def add_lines(self):
        for line in self.start.lines:
            line.purchase = Transaction().context.get('active_id', False)
            line.save()

    def transition_add_new_(self):
        self.add_lines()
        return 'start'

    def transition_add_(self):
        self.add_lines()
        return 'end'


class PurchasePaymentForm(metaclass=PoolMeta):
    __name__ = 'purchase.payment.form'
    self_pick_up = fields.Boolean('Self Pick Up', readonly=True)

    @classmethod
    def view_attributes(cls):
        return super(PurchasePaymentForm, cls).view_attributes() + [
            ('//label[@id="self_pick_up_note1"]', 'states', {
                    'invisible': ~Eval('self_pick_up', False),
                    }),
            ('//label[@id="self_pick_up_note2"]', 'states', {
                    'invisible': ~Eval('self_pick_up', False),
                    }),
            ('//separator[@id="workflow_notes"]', 'states', {
                    'invisible': ~Eval('self_pick_up', False),
                    })]


class WizardPurchasePayment(metaclass=PoolMeta):
    __name__ = 'purchase.payment'
    print_ = StateTransition()
    print = StateReport('purchase_pop.purchase_ticket')

    def default_start(self, fields):
        Purchase = Pool().get('purchase.purchase')
        purchase = Purchase(Transaction().context['active_id'])
        result = super(WizardPurchasePayment, self).default_start(fields)
        result['self_pick_up'] = purchase.self_pick_up
        return result

    def transition_pay_(self):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')
        active_id = Transaction().context.get('active_id', False)
        purchase = Purchase(active_id)
        result = super(WizardPurchasePayment, self).transition_pay_()
        if purchase.shipment_state in ['waiting']:
            Purchase.full_to_done([purchase])
        return result

    def transition_print(self):
        return 'end'

    def do_print(self, action):
        data = {}
        data['id'] = Transaction().context['active_ids'].pop()
        data['ids'] = [data['id']]
        return action, data
