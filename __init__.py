# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import configuration
from . import party
from . import purchase
from . import shop


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationSequence,
        purchase.Purchase,
        purchase.PurchaseLine,
        purchase.StatementLine,
        purchase.AddProductForm,
        purchase.PurchasePaymentForm,
        shop.PurchaseShop,
        module='purchase_pop', type_='model')
    Pool.register(
        purchase.PurchaseTicketReport,
        purchase.PurchaseReportSummary,
        module='purchase_pop', type_='report')
    Pool.register(
        party.PartyReplace,
        purchase.WizardAddProduct,
        purchase.WizardPurchasePayment,
        module='purchase_pop', type_='wizard')
