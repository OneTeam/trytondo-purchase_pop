Ventana Principal
==================


.. image:: imagenes/menu/ventana_ppal.png
   :height: 100
   :width: 800
   :alt: alternate text


::

        ...


Esta es la ventana principal del Tryton, en ella encontraremos, de izquierda a derecha

- El logo de *EcooSolida* que tiene como función abrir y cerrar el menú que contiene las opciones de **Cafeteros** y **POP Purchases** (estas opciones las veremos en un momento.

- Casilla de *Acción*, este es un cuadro de búsqueda y para guardar *Favoritos*, es decir, que podemos guardar un registro que necesitemos con frecuencia.

- El siguiente ícono son las preferencias de cada punto de compra, tiene la letra incial del punto de compra y el nombre. Al hacer click se abrirá un ventana en la cual podremos cambiar algunas características del punto. 

.. image:: imagenes/menu/preferencias.png
   :height: 400
   :width: 900
   :alt: alternate text


::
  
 Recuerde dar click en Aceptar después de realizar los cambios.


- El último ícono es una flecha en un cuadro, damos click sobre él para salir del programa y terminar la sesión.
