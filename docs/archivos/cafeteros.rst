Cafeteros  
=========  
        

  .. image:: imagenes/cafeteros/cafeteros.png
        :height: 150
        :width: 700
        :alt: alternate text



En este ventana podremos visualizar los cafeteros que tenemos registrados en el sistema. En la imagen anterior no pueden verse debido a que la base de datos se encuentra vacía. Cuando se hayan registrado alguno podremos ver la información en esta ventana.

Para agregar un nuevo registro damos click en *Nuevo* (podemos ver estas opciones en **Menú General**), y se abrirá la siguiente ventana:



.. image:: imagenes/cafeteros/nuevo_cafetero.png
   :height: 300
   :width: 700
   :alt: alternate text


- Los cuadros en rojo nos muestran los campos que debemos llenar. Los primeros son simplemente los nombre y apellidos de los caficultores, en caso de que un caficultor no tenga segundo nombre u otro campo, no hay problema con dejar ese espacio sin llenar.         

        + Bajo el recuadro de *Primer Apellido* podemos observar dos pequeñas pestañas, una dice **General** y la otra **Identificadores**, tengamos esto presente, ya que al final explicaremos como se añade un documento de identidad al caficultor.



- El siguiente campo a diligenciar es el de *Calle*, en este campo solamente debemos poner la dirección del caficultor.
 
- Ahora, si bajamos un poco más se observan cuadros rojos y azules, recordemos que los rojos son campos obligatorios y los azules son listas desplegables, es decir, que no tenemos que escribir en ellos, lo que debemos hacer es dar un click en la flecha que encierra el cuadro azul y de allí nos aparecerán distintas opciones, debemos elegir la que se acomode a la información que nos suministre el caficultor. 

  - En *País* damos click en **Colombia**, es la única opción posible.

  - La *Subdivisión* es el **Departamento**, ejemplo: *Antioquia, Bolívar, Caldas, Huila* y más.

  - Al elegir la *Subdivisión* podremos ver en *Municipio*, los municipios de esa región y procedemos a seleccionar el adecuado. 

Ahora procederemos a diligenciar la forma en que podemos comunicarnos con el caficultor, en el **Tryton** se conoce como *Métodos de Contacto*, así como se ve en la imagen: 


 .. image:: imagenes/cafeteros/nuevo_metodos_de_contacto.png
   :height: 150
   :width: 700
   :alt: alternate text


Puede verse que en la parte de arriba que tiene un pequeño menú que nos sirve para agregar (**Nuevo**) y borrar (*Papelera*) registros, en este caso *Métodos de Contacto*, estos no son más que *Teléfono, celular, correo* y más.


En la siguiente imagen se ve una ventana después de haber creado un nuevo registro (click sobre el símbolo más[+] ).  



.. image:: imagenes/cafeteros/llenar_campos.png
   :height: 150
   :width: 700
   :alt: alternate text


Aquí vemos un cuadro azul y varios elementos subrayados.

- El cuadro azul se encuentra sobre la opción **Tipo**. Al dar click cobre la flecha en el recuadro se mostrarán las opciones:

  - *Telédono*: este es el teléfono fijo.
  - *Móvil*: este es el número de celular.
  - *Fax*
  - *Correo*: se usa en caso de que tenga un em@il o correo electrónico.
  - *Sitio Web*: se coloca la página web o *URL* que es el link de la página.
  - Los demás y que no tienen gran uso son: *Skype, SIP, IRC, Jabber* y *Otros*, en la opción *Otros* si tenemos que escribir que medio usa.

- El campo que se encuentra al lado de **Tipo** es *Valor*, allí escribimos el número del celular o teléfono fijo o em@il o página erb, según el **Tipo** que seleccionemos anteriormente.

- El campo siguiente es *Nombre*, puede escribirse nuevamente el nombre del caficultor.


Ahora, recordemos que existen dos pestañas en la ventana de **Cafeteros**, una es *General* y la otra es *Identificadores*.


.. image:: imagenes/cafeteros/identidad.png
   :height: 150
   :width: 500
   :alt: alternate text


Aquí podemos ver sobre el recuadro rojo que estamos sobre una pestaña llamada *Identificadores* y el otro cuadro es el submenú que tiene para agregar o quitar registros, es el mismo que en imágenes anteriores.

al dar click en *Nuevo* se abrirá otra ventana para agregar la identificación del caficultor.


.. image:: imagenes/cafeteros/identidad_nuevo.png
   :height: 150
   :width: 500
   :alt: alternate text


En esta nueva ventana observamos:

- **Tipo**: y el recuadro azul, recordemos, es para una lista de opciones. Al dar click sobre la flecha se despliega una cantidad de opciones, en la cual elegiremos la que posea el caficultor.

- Dependiendo la opción elegida en **Tipo** (NIT, cédula, pasaporte, entre otros), escribimos en **Código** el número que equivale al tipo de documento.

- Y para terminar click en **Aceptar**


::

   De esta manera se crea un nuevo cliente o caficultor para nuestra base de datos.
   Al estar creado podremos buscarlo por diferentes filtros, nombre, identificación
   y demás.


Recordemos siempre *Guardar* los cambios que hagamos en los regístros, sean modificaciones o creaciones, es decir, información nueva.
