Menú General
=============

Este menú tiene la particularidad de que podremos verlo y usar algunas de sus acciones, en cada uno de las ventanas del tryton.
Haremos entonces una breve descripción de los íconos más utilizados.
Observemos que en la siguiente imagen dichos íconos aparecen numerados, y se dará la descripción según corresponda.


::

...


.. image:: imagenes/menu/menu.png
   :height: 80
   :width: 600
   :alt: alternate text

::

...


#. *Cambio de vista:* Al accionar este ícono podremos cambiar en la forma de ver los
   los objetos. Podremos ver toda una lista registros, productos, clientes/proveedores
   (según sea el caso) y al *cambiar de vista* se visualizará la información de un
   producto, registro, cliente/proveedor. En otras palabras, ver la información de
   manera detallada sobre un item.


#. *Anterior:* Si estamos mirando la información detallada de un item, al hacer click
   se mostrará la información del item inmediatamente anterior o de arriba de la lista.


#. *Posterior:* Si estamos mirando la información detallada de un item, al hacer click
   se mostrará la información del item inmediatamente posterior o de abajo de la lista.


#. *Nuevo:* En esta opción se crean nuevos productos, clientes, usuarios y demás. Se usa
   para añadir nueva información en el manejo de la contabilidad, se usa con bastante
   frecuencia.


#. *Guardar:* Al editar algún cambio en las ventanas del tryton usaremos este ícono
   para guardar los cambios realizados.


#. *Actualizar:* Al realizar algún cambio, usaremos este ícono para recargar la ventana
   del tryton y poder observar el cabio realizado.


#. *Acciones:* Aquí dispondremos de varias acciones según la ventana que estemos utilizando
   . De ser usada en posteriores referencias, haremos una explicación más detallada de la
   misma.


**Recordemos que este menú saldrá en todas las ventanas del tryton.**

