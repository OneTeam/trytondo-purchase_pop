Pop Purchases o Compras
========================

Ahora aprenderemos a realizar una compra.

En la ventana principal aparecen dos opciones, ya vimos la de *Cafeteros*, veamos ahora la de *Pop Purchases*. Al dar click allí se nos abrirá la siguiente ventana.


.. image:: imagenes/popo/compras.png
   :height: 250
   :width: 500
   :alt: alternate text

Puede verse una cantidad de compras ya realizadas, si no se encuentran pagadas y vamos a realizar algún cambio, debemos tener presente que registro se encuentra seleccionado, pues es en ese registro donde se harán los cambios o el pago de la factura como tal.

La siguiente imagen mostrará una plantilla para la compra de un producto, esta ventana se abre después de dar click en *Nuevo*, en el submenú de la parte de arriba.


.. image:: imagenes/popo/compras_nuevo1.png
   :height: 400
   :width: 700
   :alt: alternate text


.. image:: imagenes/popo/compras_nuevo2.png
   :height: 400
   :width: 700
   :alt: alternate text


Como en las anteriores ventanas, vemos un submenú que está en el primer recuadro rojo, allí está la opción de *Nuevo* y la usaremos para registrar los productos, cada vez que damos click en *Nuevo* es solo un producto el que se agrega. 

La opción de *Nuevo* solo se habilitará cuando agreguemos a un **Tercero** o cliente, pues es a este a quien se hará la compra. Podemos ver dicho campo en el tercer cuadro rojo.

Entonces, después de agregar al **Tercero** y dar click en *Nuevo*, se mostrarán en blanco los campos donde se llenará la información de la compra.

        - *Producto*: si damos click sobre la lupita podremos buscar el producto deseado o si empezamos a escribir sobre el campo, se desplegará la siguienre lista:




.. image:: imagenes/popo/compras_productos.png
   :height: 350
   :width: 250
   :alt: alternate text


Luego de seleccionar el producto, ingresamos la cantidad (10 Kilogramos, 30, 100, 500...), la unidad ya se encuentra en Kilogramos, entonces dejamos ese campo como esta. Damos click en *Guardar* y luego cambiamos el precio del café según el precio en el que se encuentre en ese momento.

En la siguiente imagen vemos los campos ya diligenciados.



.. image:: imagenes/popo/compras_proceso.png
   :height: 200
   :width: 600
   :alt: alternate text


Y al final tenemos los dos últimos recuadros rojos, uno es el **TIQUE** y lo usaremos para imprimir un recibo o comprobante de pago. El otro recuadro rojo, **PAGAR** lo usaremos para hacer efectivo el pago del producto. Al hacerlo se abrirá la siguiente vetana:


.. image:: imagenes/popo/compras_pagar.png
   :height: 200
   :width: 500
   :alt: alternate text



En esta ventana verificamos la información de la compra, el valor y el cliente. Si esta todo bien damos click en **Pagar** y la compra quedará hecha, en este punto ya no podremos volver atrás y la compra se almacenará en el sistema contable **TrytonERP**.
