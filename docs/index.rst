.. One/Cluster documentation master file, created by
   sphinx-quickstart on Thu Aug 12 10:56:38 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

¡Documentación Puntos de Compra!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contenido:


   archivos/menu
   archivos/ventana
   archivos/cafeteros
   archivos/popo


¡Atención!
===========

Esta página tiene la documentación necesaria para manejar los diferentes puntos
de compra con el **TrytonERP**. Por favor lealo desde el principio, allí encontrará 
información importante sobre algunas funciones que tiene el programa y también sobre
la distribución de las ventanas e íconos.

Si tiene alguna sugerencia sobre el manual, ya sea en el contenido (si falta algo) o 
en la forma en que esta escrito (si se puede explicar mejor), contactenos por medio
de los correos info@onecluster.org y también a ecoosolida@gmail.com

¡Saludos y muchos exitos!





* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
